# coding=utf-8
import matplotlib.pyplot as plt
from math import sqrt, sin, cos, pi
from numpy import arange


def print_menu():
    print('1 - ton prosty')
    print('2 - suma tonów')
    print('3 - różnica tonów')
    return input()


def get_time_and_sampling():
    t = float(input('Czas: '))
    f = float(input('Częstotliwość próbkowania: '))
    return arange(0, t, 1 / f), f


def get_tone_params():
    a = float(input('Amplituda: '))
    b = float(input('Częstotliwość: '))
    c = float(input('Faza: '))
    return a, b, c


def pure_tone(t, amp, freq, phase):
    a_freq = 2 * pi * freq
    return amp * sin(t * a_freq + phase)


def transform_values(domain, amp, freq, phase):
    return list(map(lambda el: pure_tone(el, amp, freq, phase), domain))


def sum_tones(first, second):
    for index, val in enumerate(first):
        first[index] = first[index] + second[index]
    return first


def subtract_tones(first, second):
    for index, val in enumerate(first):
        first[index] = first[index] - second[index]
    return first


def eval_tone(choice, domain):
    if choice == '1':
        print('Parametry tonu:')
        args = get_tone_params()
        return transform_values(domain, *args)
    elif choice == '2':
        print('Parametry 1 tonu:')
        args1 = get_tone_params()
        y1 = transform_values(domain, *args1)
        print('Parametry 2 tonu:')
        args2 = get_tone_params()
        y2 = transform_values(domain, *args2)
        return sum_tones(y1, y2)
    elif choice == '3':
        print('Parametry 1 tonu:')
        args1 = get_tone_params()
        y1 = transform_values(domain, *args1)
        print('Parametry 2 tonu:')
        args2 = get_tone_params()
        y2 = transform_values(domain, *args2)
        return subtract_tones(y1, y2)
    else:
        raise ValueError('no such option')


def calc_one_output_dft(signal_samples, k):
    n = len(signal_samples)
    t_arg = (2 * pi * k) / n
    real = sum(map(lambda val: val[1] * cos(t_arg*val[0]), enumerate(signal_samples)))
    im = sum(map(lambda val: val[1] * sin(t_arg*val[0]), enumerate(signal_samples)))
    return sqrt(real**2 + im**2)/n


def calc_dft(signal_samples):
    return list(map(lambda k: calc_one_output_dft(signal_samples, k), range(len(signal_samples))))


userSelect = print_menu()
# PERIOD = None
tone_domain, FREQ = get_time_and_sampling()
tone_values = eval_tone(userSelect, tone_domain)
dft = (calc_dft(tone_values))
# print(dft)

plt.subplot(2, 1, 1)
plt.plot(tone_domain, tone_values)
plt.ylabel('WYKRES TONU PROSTEGO')

plt.subplot(2, 1, 2)
plt.bar(range(len(tone_values)), dft)
plt.ylabel('WIDMO AMPLITUDOWE')
plt.show()
