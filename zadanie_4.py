from tkinter import *
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg as Canvas
from math import sqrt, sin, cos, pi
from numpy import arange, random


selected_keying = None
info_signal_chart = None
output_signal_chart = None
dft_signal_chart = None

user_inputs = {
    'SYM_TIME': 'simulation time:',
    'SAMPLING': 'sampling:',
    'FREQUENCY': 'frequency:',
    'f': 'carrier frequency',
    'N': 'N - frequency factor'
}

keying_types = [
    'ASK',
    'FSK',
    'PSK',
]


def init_radio_value():
    global selected_keying
    selected_keying = StringVar()
    selected_keying.set('ASK')


def make_radio(container):
    keying_container = Frame(container)
    for text in keying_types:
        b1 = Radiobutton(keying_container, text=text,
                         variable=selected_keying, value=text)
        b1.pack(side=LEFT, anchor=W)
    keying_container.pack()


def make_frames(root):
    user_input_frame = Frame(root)
    form_frame = Frame(user_input_frame)
    buttons_frame = Frame(user_input_frame)
    plot_frame = Frame(root)
    user_input_frame.pack(side=LEFT)
    plot_frame.pack(side=RIGHT)
    form_frame.pack(side=TOP)
    buttons_frame.pack(side=BOTTOM)
    return form_frame, buttons_frame, plot_frame


def make_form(container, fields, header):
    entries = {}
    label = Label(container, text=header)
    label.pack(side=TOP)
    for field in fields:
        row = Frame(container)
        label = Label(row, width=20, text=fields.get(field), anchor='w')
        val_input = Entry(row)
        row.pack(side=TOP, fill=X, padx=5, pady=5)
        label.pack(side=LEFT)
        val_input.pack(side=RIGHT, expand=YES)
        entries[field] = val_input
    return entries


def make_buttons(container, controls):
    btn_calc = Button(container,
                      text='Calculate',
                      command=lambda e=controls: calculate_functions(e))
    btn_calc.pack(side=BOTTOM, padx=5, pady=10)


def make_charts(container):
    info_signal_chart = Figure(figsize=(8, 2), dpi=100)
    output_signal_chart = Figure(figsize=(8, 2), dpi=100)
    dft_signal_chart = Figure(figsize=(8, 2), dpi=100)
    info_signal_chart.add_subplot(111)
    output_signal_chart.add_subplot(111)
    dft_signal_chart.add_subplot(111)
    Canvas(info_signal_chart, master=container).get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)
    Canvas(output_signal_chart, master=container).get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)
    Canvas(dft_signal_chart, master=container).get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)
    return info_signal_chart, output_signal_chart, dft_signal_chart


def draw_functions(domain, input_values, output_values, dft_values, sim_time):
    info_signal_chart.get_axes()[0].cla()
    output_signal_chart.get_axes()[0].cla()
    dft_signal_chart.get_axes()[0].cla()
    info_signal_chart.get_axes()[0].plot(domain, input_values)
    output_signal_chart.get_axes()[0].plot(domain, output_values)
    freq_domain = list(map(lambda x: x/sim_time, range(len(domain))))
    dft_signal_chart.get_axes()[0].bar(freq_domain, dft_values)
    info_signal_chart.canvas.draw()
    output_signal_chart.canvas.draw()
    dft_signal_chart.canvas.draw()


def calculate_functions(sim_params):
    domain, sig_freq, sim_time, freq, n = get_simulation_params(sim_params)
    input_signal = generate_digital_signal(sim_time, sig_freq, domain)
    output_signal = signal_keying(domain, input_signal, sig_freq, freq, n)
    dft = calc_dft(output_signal)
    draw_functions(domain, input_signal, output_signal, dft, sim_time)


def get_simulation_params(values):
    sim_time = float(values['SYM_TIME'].get())
    sampling = float(values['SAMPLING'].get())
    sig_freq = float(values['FREQUENCY'].get())
    freq = float(values['f'].get())
    n = float(values['N'].get())
    domain = arange(0, sim_time, 1 / sampling)
    return domain, sig_freq, sim_time, freq, n


def generate_digital_signal(sim_time, freq, domain):
    signal = random.randint(2, size=int(sim_time*freq))
    time_of_bit = int(len(domain) / (sim_time*freq))
    result = []
    for sig in signal:
        result = result + [sig]*time_of_bit
    return result


def signal_keying(domain, input_signal, sig_freq, freq, n):
    signal = zip(domain, input_signal)
    f1 = sig_freq * (n+1)
    f2 = sig_freq * (n + 2)
    if selected_keying.get() == 'ASK':
        return list(map(lambda t: t[1]*sin(2*pi*freq*t[0]), signal))
    elif selected_keying.get() == 'FSK':
        return list(map(lambda t: sin(2*pi*f1*t[0]) if t[1] == 0 else sin(2*pi*f2*t[0]), signal))
    elif selected_keying.get() == 'PSK':
        return list(map(lambda t: sin(2*pi*freq*t[0]) if t[1] == 0 else sin(2*pi*freq*t[0]+pi), signal))


def calc_one_output_dft(signal_samples, k):
    n = len(signal_samples)
    t_arg = (2 * pi * k) / n
    real = sum(map(lambda val: val[1] * cos(t_arg*val[0]), enumerate(signal_samples)))
    im = sum(map(lambda val: val[1] * sin(t_arg*val[0]), enumerate(signal_samples)))
    return sqrt(real**2 + im**2)/n


def calc_dft(signal_samples):
    return list(map(lambda k: calc_one_output_dft(signal_samples, k), range(len(signal_samples))))


if __name__ == '__main__':
    root = Tk()
    init_radio_value()
    form_frame, buttons_frame, plot_frame = make_frames(root)
    make_radio(form_frame)
    form_controls = make_form(form_frame, user_inputs, 'Simulation params:')
    info_signal_chart, output_signal_chart, dft_signal_chart = make_charts(plot_frame)
    make_buttons(buttons_frame, form_controls)
    root.mainloop()
