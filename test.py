from numpy import arange
import matplotlib.pyplot as plt

domain = arange(0, 10, 0.01)
dx = (domain[len(domain) - 1] - domain[0]) / len(domain)
print(dx)

y = list(map(lambda x: 2 * x, domain))

f_sum = 0
for i in domain:
    f_sum += i * 2 * dx

print(f_sum)

plt.plot(domain, y)
plt.ylabel('WYKRES TONU PROSTEGO')
plt.show()
