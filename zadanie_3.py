from tkinter import *
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg as Canvas
from math import sqrt, sin, cos, pi
from numpy import arange

carrier_signal_params = {}
selected_option = None
modulation_option = None
carrier_signal_chart = None
info_signal_chart = None
dft_signal_chart = None

user_inputs = {
    'SYM_TIME': 'simulation time:',
    'SAMPLING': 'sampling:',
    'AMPLITUDE': 'amplitude:',
    'FREQUENCY': 'frequency:',
    'PHASE': 'phase:',
    'MODULATION_RATIO': 'modulation ratio (k):',
}

simple_tone_inputs = {
    'AMPLITUDE': 'amplitude:',
    'FREQUENCY': 'frequency:',
    'PHASE': 'phase:',
}

modes = [
    'simple',
    'addition',
    'subtraction',
]

modulation_types = [
    'AM',
    'PM',
    'FM',
]


def pure_tone(t, amp, freq, phase):
    a_freq = 2 * pi * freq
    return amp * sin(t * a_freq + phase)


def transform_values(params, domain):
    amp = params['AMPLITUDE']
    freq = params['FREQUENCY']
    phase = params['PHASE']
    return list(map(lambda x: pure_tone(x, amp, freq, phase), domain))


def sum_tones(first, second):
    for index, val in enumerate(first):
        first[index] = first[index] + second[index]
    return first


def subtract_tones(first, second):
    for index, val in enumerate(first):
        first[index] = first[index] - second[index]
    return first


def calc_one_output_dft(signal_samples, k):
    n = len(signal_samples)
    t_arg = (2 * pi * k) / n
    real = sum(map(lambda val: val[1] * cos(t_arg*val[0]), enumerate(signal_samples)))
    im = sum(map(lambda val: val[1] * sin(t_arg*val[0]), enumerate(signal_samples)))
    return sqrt(real**2 + im**2)/n


def calc_dft(signal_samples):
    return list(map(lambda k: calc_one_output_dft(signal_samples, k), range(len(signal_samples))))


def modulate_signal(domain, info, k):
    amp = carrier_signal_params['AMPLITUDE']
    freq = carrier_signal_params['FREQUENCY']
    signal = zip(domain, info)
    if modulation_option.get() == 'AM':
        return list(map(lambda t: amp * cos(2 * pi * freq * t[0]) * (k * t[1] + 1), signal))
    elif modulation_option.get() == 'PM':
        return list(map(lambda t: amp * cos(2 * pi * freq * t[0] + k * t[1]), signal))
    elif modulation_option.get() == 'FM':
        dx = (domain[len(domain)-1] - domain[0]) / len(domain)
        f_sum = 0

        def calc(t):
            nonlocal f_sum
            f_sum += t[1] * dx
            return amp * cos(2 * pi * (freq * t[0] + k * f_sum))

        return list(map(lambda t: calc(t), signal))


def make_frames(root):
    user_input_frame = Frame(root)
    form_frame = Frame(user_input_frame)
    buttons_frame = Frame(user_input_frame)
    plot_frame = Frame(root)
    user_input_frame.pack(side=LEFT)
    plot_frame.pack(side=RIGHT)
    form_frame.pack(side=TOP)
    buttons_frame.pack(side=BOTTOM)
    return form_frame, buttons_frame, plot_frame


def make_form(container, fields, header):
    entries = {}
    label = Label(container, text=header)
    label.pack(side=TOP)
    for field in fields:
        row = Frame(container)
        label = Label(row, width=20, text=fields.get(field), anchor='w')
        val_input = Entry(row)
        row.pack(side=TOP, fill=X, padx=5, pady=5)
        label.pack(side=LEFT)
        val_input.pack(side=RIGHT, expand=YES)
        entries[field] = val_input
    return entries


def make_buttons(container, controls, tones):
    btn_calc = Button(container,
                      text='Calculate',
                      command=lambda e=controls, t=tones: calculate_functions(e, t))
    btn_calc.pack(side=BOTTOM, padx=5, pady=10)


def make_radio(container):
    opt_container = Frame(container)
    for text in modes:
        b1 = Radiobutton(opt_container, text=text,
                        variable=selected_option, value=text)
        b1.pack(side=LEFT, anchor=W)
    opt_container.pack()
    mod_container = Frame(container)
    for text in modulation_types:
        b2 = Radiobutton(mod_container, text=text,
                         variable=modulation_option, value=text)
        b2.pack(side=LEFT, anchor=W)
    mod_container.pack()


def init_radio_values():
    global selected_option
    global modulation_option
    selected_option = StringVar()
    selected_option.set('simple')
    modulation_option = StringVar()
    modulation_option.set('AM')


def make_charts(container):
    carrier_signal_chart = Figure(figsize=(8, 2), dpi=100)
    info_signal_chart = Figure(figsize=(8, 2), dpi=100)
    dft_signal_chart = Figure(figsize=(8, 2), dpi=100)
    carrier_signal_chart.add_subplot(111)
    info_signal_chart.add_subplot(111)
    dft_signal_chart.add_subplot(111)
    Canvas(carrier_signal_chart, master=container).get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)
    Canvas(info_signal_chart, master=container).get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)
    Canvas(dft_signal_chart, master=container).get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)
    return carrier_signal_chart, info_signal_chart, dft_signal_chart


def calculate_functions(sim_params, tones_params):
    domain, ratio = get_carrier_signal_values(sim_params)
    tones_params = list(map(lambda x: get_simple_tone_values(x), tones_params))
    tone_values = list(map(lambda x: transform_values(x, domain), tones_params))
    if selected_option.get() == 'simple':
        tone_values = tone_values[0]
    elif selected_option.get() == 'addition':
        tone_values = sum_tones(tone_values[0], tone_values[1])
    elif selected_option.get() == 'subtraction':
        tone_values = subtract_tones(tone_values[0], tone_values[1])
    modulated_signal = modulate_signal(domain, tone_values, ratio)
    dft = calc_dft(modulated_signal)
    draw_functions(domain, tone_values, modulated_signal, dft)


def draw_functions(domain, carrier_values, info_values, dft_values):
    carrier_signal_chart.get_axes()[0].cla()
    info_signal_chart.get_axes()[0].cla()
    dft_signal_chart.get_axes()[0].cla()
    carrier_signal_chart.get_axes()[0].plot(domain, carrier_values)
    info_signal_chart.get_axes()[0].plot(domain, info_values)
    dft_signal_chart.get_axes()[0].bar(range(len(domain)), dft_values)
    carrier_signal_chart.canvas.draw()
    info_signal_chart.canvas.draw()
    dft_signal_chart.canvas.draw()


def get_carrier_signal_values(values):
    sim_time = float(values['SYM_TIME'].get())
    sampling = float(values['SAMPLING'].get())
    ratio = float(values['MODULATION_RATIO'].get())
    domain = arange(0, sim_time, 1 / sampling)
    carrier_signal_params['AMPLITUDE'] = float(values['AMPLITUDE'].get())
    carrier_signal_params['FREQUENCY'] = float(values['FREQUENCY'].get())
    carrier_signal_params['PHASE'] = float(values['PHASE'].get())
    return domain, ratio


def get_simple_tone_values(controls):
    return {
        'AMPLITUDE': float(controls['AMPLITUDE'].get() or 0),
        'FREQUENCY': float(controls['FREQUENCY'].get() or 0),
        'PHASE': float(controls['PHASE'].get() or 0),
    }


if __name__ == '__main__':
    root = Tk()
    init_radio_values()
    # scrollbar = Scrollbar(root)
    # scrollbar.pack(side=RIGHT, fill=Y)
    form_frame, buttons_frame, plot_frame = make_frames(root)
    make_radio(form_frame)
    form_controls = make_form(form_frame, user_inputs, 'Carrier wave:')
    tone1_form = make_form(form_frame, simple_tone_inputs, 'TONE 1:')
    tone2_form = make_form(form_frame, simple_tone_inputs, 'TONE 2:')
    carrier_signal_chart, info_signal_chart, dft_signal_chart = make_charts(plot_frame)
    make_buttons(buttons_frame, form_controls, [tone1_form, tone2_form])
    root.mainloop()
