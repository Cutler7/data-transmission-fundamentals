# coding=utf-8
import matplotlib.pyplot as plt
from math import sin, pi
from numpy import arange


def print_menu():
    print('1 - ton prosty')
    print('2 - suma tonów')
    print('3 - różnica tonów')
    return input()


def get_time_and_sampling():
    t = float(input('Czas: '))
    f = float(input('Częstotliwość próbkowania: '))
    return arange(0, t, 1 / f)


def get_tone_params():
    a = float(input('Amplituda: '))
    b = float(input('Częstotliwość: '))
    c = float(input('Faza: '))
    return a, b, c


def pure_tone(t, A, freq, phase):
    a_freq = 2 * pi * freq
    return A * sin(t * a_freq + phase)


def transform_values(A, freq, phase):
    y = list(map(lambda x: pure_tone(x, A, freq, phase), domain))
    return domain, y


def sum_tones(first, second):
    for index, val in enumerate(first):
        first[index] = first[index] + second[index]
    return first


def subtract_tones(first, second):
    for index, val in enumerate(first):
        first[index] = first[index] - second[index]
    return first


choice = print_menu()
domain = get_time_and_sampling()
x1 = 0
x2 = 0
y1 = 0
y2 = 0

if choice == '1':
    print('Parametry tonu:')
    args = get_tone_params()
    [x1, y1] = transform_values(*args)
elif choice == '2':
    print('Parametry 1 tonu:')
    args1 = get_tone_params()
    [x1, y1] = transform_values(*args1)
    print('Parametry 2 tonu:')
    args2 = get_tone_params()
    [x2, y2] = transform_values(*args2)
    y1 = sum_tones(y1, y2)
elif choice == '3':
    print('Parametry 1 tonu:')
    args1 = get_tone_params()
    [x1, y1] = transform_values(*args1)
    print('Parametry 2 tonu:')
    args2 = get_tone_params()
    [x2, y2] = transform_values(*args2)
    y1 = subtract_tones(y1, y2)
else:
    print('no such option')

plt.plot(x1, y1)
plt.ylabel('WYKRES TONU PROSTEGO')
plt.show()
